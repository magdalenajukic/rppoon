using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON
{
    class ToDoList
    {
        private List<Note> Tasks = new List<Note>();

        public void AddTask(Note task)
        {
            Tasks.Add(task);
        }

        public void RemoveTask(Note task)
        {
            Tasks.Remove(task);

        }

        public Note GetTask(int index)
        {
            return Tasks[index];
        }


        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            for(int i=0;i<Tasks.Count;i++)
            {
                stringBuilder.Append(Tasks[i]).Append("\n");
            }
            return stringBuilder.ToString();
        }

        public void FinishedTasks()
        {
            for(int i = 0; i < Tasks.Count; i++)
            {
                if (Tasks[i].ImportanceLevel == 1)
                {
                    RemoveTask(Tasks[i]);
                }
            }
        }

    }
}