using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RPPOON
{
    class TimeNote : Note
    { 
        public DateTime Time { get; set; }

        public TimeNote(string author, string text, int level, DateTime time):base(author, text, level)
        {
            Time = time;
        }
        public override string ToString()
        {
            return base.ToString()+", "+this.Time;
        }
    }
}
