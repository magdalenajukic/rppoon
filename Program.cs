using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note = new Note("Fran", "ljubav i gljive", 4);
            Note note1 = new Note();
            Note note2 = new Note("Petar");
            Note note3 = new Note("Matija", "primjer", 1);
            Note note4 = new Note("Pavo", "neki zadatak",3);
            Note note5 = new Note("Gusak", "primjer", 1);

            note1.Text="Torba ili ruksak?";
            note1.ImportanceLevel=3;
            note2.Text="Pozdrav suncu!";

            Console.WriteLine(note.ToString());
            Console.WriteLine(note1.ToString());
            Console.WriteLine(note2.ToString());
            Console.WriteLine(note3.ToString());
            Console.WriteLine(note4.ToString());
            Console.WriteLine(note5.ToString());

            TimeNote novo = new TimeNote("Fran", "ljubav i gljive", 1, new DateTime(2008, 12, 2, 12, 33, 56));
            Console.WriteLine(novo.ToString());

            ToDoList list = new ToDoList();
            list.AddTask(note);
            list.AddTask(note1);
            list.AddTask(note2);
            list.AddTask(note3);
            list.AddTask(note4);
            list.AddTask(note5);

            Console.WriteLine(list.ToString());
            list.FinishedTasks();
            Console.WriteLine("\n");
            Console.WriteLine(list.ToString());
        }
    }
}