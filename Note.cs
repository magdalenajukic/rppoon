using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON
{ 
    class Note {

        private string mAuthor;
        private string mText;
        private int mImportanceLevel;

        //public void setAuthor(string author) { this.mAuthor = author; }
        //public void setText(string text) { this.mText = text; }
        //public void setImportanceLevel(int importanceLevel) { this.mImportanceLevel = importanceLevel; }

        public string getAuthor() { return this.mAuthor; }
        public string getText() { return this.mText; }
        public int getImportanceLevel() { return this.mImportanceLevel; }

        public string Author { get; private set; }
        public string Text { get; set; }
        public int ImportanceLevel { get; set; }

        public Note(string author, string text, int level)
        {
            this.Author = author;
            this.Text = text;
            if(level<=5 && level >= 1)
            {
                this.ImportanceLevel = level;
            }
        }
        public Note()
        {
            this.Author = "-";
            this.Text = "-";
            this.ImportanceLevel = 5;
        }
        public Note(string author)
        {
            this.Author = author;
            this.Text = "-";
            this.ImportanceLevel = 5;
        }

        public override string ToString()
        {
            return this.Author + ",\t" + this.Text + ",\t" + this.ImportanceLevel;
        }

    }
}

